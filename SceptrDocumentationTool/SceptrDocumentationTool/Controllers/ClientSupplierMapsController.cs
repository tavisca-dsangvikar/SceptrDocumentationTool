using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSupplierMapper.Models;

namespace SceptrDocumentationTool.Controllers
{   
    public class ClientSupplierMapsController : Controller
    {
        private ClientSupplierContext context = new ClientSupplierContext();

        //
        // GET: /ClientSupplierMaps/

        public ViewResult Index()
        {
            return View(context.ClientSupplierMaps.Include(clientsuppliermap => clientsuppliermap.Client).Include(clientsuppliermap => clientsuppliermap.Supplier).Include(clientsuppliermap => clientsuppliermap.Product).ToList());
        }

        //
        // GET: /ClientSupplierMaps/Details/5

        public ViewResult Details(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            return View(clientsuppliermap);
        }

        //
        // GET: /ClientSupplierMaps/Create

        public ActionResult Create()
        {
            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View();
        } 

        //
        // POST: /ClientSupplierMaps/Create

        [HttpPost]
        public ActionResult Create(ClientSupplierMap clientsuppliermap)
        {
            if (ModelState.IsValid)
            {
                context.ClientSupplierMaps.Add(clientsuppliermap);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View(clientsuppliermap);
        }
        
        //
        // GET: /ClientSupplierMaps/Edit/5
 
        public ActionResult Edit(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View(clientsuppliermap);
        }

        //
        // POST: /ClientSupplierMaps/Edit/5

        [HttpPost]
        public ActionResult Edit(ClientSupplierMap clientsuppliermap)
        {
            if (ModelState.IsValid)
            {
                context.Entry(clientsuppliermap).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View(clientsuppliermap);
        }

        //
        // GET: /ClientSupplierMaps/Delete/5
 
        public ActionResult Delete(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            return View(clientsuppliermap);
        }

        //
        // POST: /ClientSupplierMaps/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            context.ClientSupplierMaps.Remove(clientsuppliermap);
            context.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult Search()
        {
            ViewBag.Message = "Supplier Documentation Tool";

            return View();
        }

        [ActionName("SearchBySupplier")]
        public ActionResult SearchBySupplier(string Name)
        {
            //string supplierName = supplier1.Name;
            var supplierList = new List<string>();

            var suppliers = from s in context.Suppliers select s.Name;

            ViewBag.supplierName = new SelectList(supplierList);

            var reqdResult = from t in context.ClientSupplierMaps select t;

            if (!String.IsNullOrEmpty(Name))
            {
                int suppId = -1;
                var supplier = from s in context.Suppliers where s.Name == Name select s;
                foreach (var row in supplier)
                {
                    suppId = row.ID;
                }
                if (suppId != -1)
                {
                    var clients = from rec in context.ClientSupplierMaps where rec.Supplier.Name.Contains(Name) select rec;//w/o sup name
                    return View(clients);
                }
            }
            return View(reqdResult);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}