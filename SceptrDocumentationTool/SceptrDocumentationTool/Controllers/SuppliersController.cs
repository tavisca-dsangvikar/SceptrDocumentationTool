using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSupplierMapper.Models;

namespace SceptrDocumentationTool.Controllers
{   
    public class SuppliersController : Controller
    {
        private ClientSupplierContext context = new ClientSupplierContext();

        //
        // GET: /Suppliers/

        public ViewResult Index()
        {
            return View(context.Suppliers.Include(supplier => supplier.Clients).ToList());
        }

        //
        // GET: /Suppliers/Details/5

        public ViewResult Details(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            return View(supplier);
        }

        //
        // GET: /Suppliers/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Suppliers/Create

        [HttpPost]
        public ActionResult Create(Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                context.Suppliers.Add(supplier);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(supplier);
        }
        
        //
        // GET: /Suppliers/Edit/5
 
        public ActionResult Edit(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            return View(supplier);
        }

        //
        // POST: /Suppliers/Edit/5

        [HttpPost]
        public ActionResult Edit(Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                context.Entry(supplier).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(supplier);
        }

        //
        // GET: /Suppliers/Delete/5
 
        public ActionResult Delete(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            return View(supplier);
        }

        //
        // POST: /Suppliers/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            context.Suppliers.Remove(supplier);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
        //public ActionResult Search()
        //{
        //    ViewBag.Message = "Supplier Documentation Tool";

        //    return View();
        //}
        //[ActionName("SearchBySupplier")]
        //public ActionResult SearchBySupplier(string supplierName)
        //{
        //    var supplierList = new List<string>();

        //    var suppliers = from s in context.Suppliers select s.Name;

        //    ViewBag.supplierName = new SelectList(supplierList);

        //    var reqdResult = from t in context.ClientSupplierMaps select t;

        //    if (!String.IsNullOrEmpty(supplierName))
        //    {
        //        int suppId = -1;
        //        var supplier = from s in context.Suppliers where s.Name == supplierName select s;
        //        //supplier = supplier.Where(s => s.Name.Contains(supplierName));
        //        //var result = new List<ClientSupplierMap>();
        //        foreach (var row in supplier)
        //        {
        //            suppId = row.ID;
        //        }
        //        if (suppId != -1)
        //        {
        //            var clients = from rec in context.ClientSupplierMaps where rec.Supplier.Name.Contains(supplierName) select rec;//w/o sup name
        //            return View(clients);
        //        }
        //    }
        //    return View(reqdResult);
        //}
    }
}