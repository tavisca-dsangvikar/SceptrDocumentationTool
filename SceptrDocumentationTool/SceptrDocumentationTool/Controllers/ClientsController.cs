using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSupplierMapper.Models;

namespace SceptrDocumentationTool.Controllers
{   
    public class ClientsController : Controller
    {
        private ClientSupplierContext context = new ClientSupplierContext();

        //
        // GET: /Clients/

        public ViewResult Index()
        {
            return View(context.Clients.Include(client => client.Suppliers).ToList());
        }

        //
        // GET: /Clients/Details/5

        public ViewResult Details(int id)
        {
            Client client = context.Clients.Single(x => x.ID == id);
            return View(client);
        }

        //
        // GET: /Clients/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Clients/Create

        [HttpPost]
        public ActionResult Create(Client client)
        {
            if (ModelState.IsValid)
            {
                context.Clients.Add(client);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(client);
        }
        
        //
        // GET: /Clients/Edit/5
 
        public ActionResult Edit(int id)
        {
            Client client = context.Clients.Single(x => x.ID == id);
            return View(client);
        }

        //
        // POST: /Clients/Edit/5

        [HttpPost]
        public ActionResult Edit(Client client)
        {
            if (ModelState.IsValid)
            {
                context.Entry(client).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(client);
        }

        //
        // GET: /Clients/Delete/5
 
        public ActionResult Delete(int id)
        {
            Client client = context.Clients.Single(x => x.ID == id);
            return View(client);
        }

        //
        // POST: /Clients/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = context.Clients.Single(x => x.ID == id);
            context.Clients.Remove(client);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}