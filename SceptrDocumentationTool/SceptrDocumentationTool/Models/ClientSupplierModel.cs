﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClientSupplierMapper.Models
{

    public class Client
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<Supplier> Suppliers { get; set; }

    }

    public class Supplier
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<Client> Clients { get; set; }

    }

    public class Product
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
    }

    public class ClientSupplierMap
    {
        [Key]
        public int Id { get; set; }

        public int ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }

        public int SupplierId { get; set; }
        [ForeignKey("SupplierId")]
        public virtual Supplier Supplier { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }

    public class ClientSupplierContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<ClientSupplierMap> ClientSupplierMaps { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}

